use crate::syllabify as syllabify_lib;
use itertools::Itertools;
use std::ops::Deref;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn accent_marker() -> char {
    syllabify_lib::ACCENT_MARKER
}

#[wasm_bindgen]
pub fn greet(name: &str) {
    alert(&format!("Buongiorno, {name}!"));
}

#[wasm_bindgen]
pub fn count_metric_syllables(line: &str) -> usize {
    wasm_count_metric_syllables(line)
}

#[wasm_bindgen]
pub fn syllabify(line: &str) -> Result<JsValue, JsValue> {
    let syllables = wasm_syllabify(line);
    Ok(serde_wasm_bindgen::to_value(&syllables)?)
}

#[wasm_bindgen]
pub fn format(text: &str) -> String {
    text.lines()
        .map(|s| syllabify_lib::format(s).unwrap_or(String::from("[???]\n")))
        .join("\n")
}

fn wasm_syllabify(line: &str) -> Vec<String> {
    syllabify_lib::syllabify(line)
        .into_iter()
        .map(|s| String::from(*s.deref()))
        .collect()
}

fn wasm_count_metric_syllables(line: &str) -> usize {
    let syllables = syllabify_lib::syllabify(line);
    syllabify_lib::count_metric_syllables(&syllables)
}

#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let _body = document.body().expect("document should have a body");
    Ok(())
}
