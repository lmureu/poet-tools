mod syllabify;
pub use syllabify::{
    count_metric_syllables, format, syllabify, Syllable, Syllables, ACCENT_MARKER,
};

#[cfg(feature = "build-wasm")]
mod webapp;
