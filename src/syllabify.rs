use anyhow::Result;
use once_cell::sync::Lazy;
use regex::Regex;
use std::fmt::{Display, Formatter};
use std::io::Write;
use std::ops::Deref;

pub struct Syllable<'a>(&'a str);
pub type Syllables<'a> = Vec<Syllable<'a>>;

pub const ACCENT_MARKER: char = '\u{03_32}';

static SYLLABIFY_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?i:([äëïöü]\u0332?)|([^aeiouàèéìòùh]*[aeiouàèéìòùh\u0332\s]+([^aeiouàèéìòùhäëïöü]+(\s|$))?))",
    )
    .unwrap()
});

pub fn syllabify(line: &str) -> Syllables {
    SYLLABIFY_REGEX
        .find_iter(line)
        .map(|m| Syllable(m.as_str().trim()))
        .collect()
}

pub fn find_last_accented_syllable(syllables: &Syllables) -> Option<usize> {
    for (index, syllable) in syllables.iter().enumerate().rev() {
        if syllable.contains(ACCENT_MARKER) {
            return Some(index + 2);
        }
    }

    None
}

pub fn count_metric_syllables(syllables: &Syllables) -> usize {
    find_last_accented_syllable(syllables).unwrap_or(syllables.len())
}

fn itt<T>(condition: bool, first: T, second: T) -> T {
    if condition {
        first
    } else {
        second
    }
}

pub fn format(verse: &str) -> Result<String> {
    let mut buffer = Vec::new();

    if verse.is_empty() {
        return Ok(String::new());
    }

    let matches = syllabify(verse);
    let metric_syllables_count = count_metric_syllables(&matches);

    write!(&mut buffer, "[{}] ", metric_syllables_count)?;

    for (index, syllable) in matches.iter().enumerate() {
        write!(&mut buffer, "{}{}", itt(index == 0, "", " | "), syllable)?;
    }

    Ok(String::from_utf8(buffer)?)
}

impl<'a> Deref for Syllable<'a> {
    type Target = &'a str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Display for Syllable<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
        //write!(f, "{}", self.0.replace(ACCENT_MARKER, ""))
    }
}
