import init, {format, accent_marker} from './pkg/poet_tools_lib.js';

(async function () {
    await init();
    console.log("loaded wasm")

    const localStorageInputKey = "input_text";
    const accent = accent_marker();
    const input = document.getElementById("input");
    const output = document.getElementById("output");
    const accent_btn = document.getElementById("accent-btn");
    const refresh_btn = document.getElementById("refresh-btn");
    const load_btn = document.getElementById("load-btn");

    function typeInTextarea(newText) {
        const [start, end] = [input.selectionStart, input.selectionEnd];
        input.setRangeText(newText, start, end, 'select');
    }

    function storeInput() {
        localStorage.setItem(localStorageInputKey, input.value);
    }

    function loadInput() {
        input.value = localStorage.getItem(localStorageInputKey);
    }

    function updateOutput() {
        storeInput();
        output.value = format(input.value);
    }

    const inputHandler = (_event) => {
        updateOutput();
    };

    const loadHandler = (_event) => {
        loadInput();
    };

    const insertAccentHandler = (_event) => {
        typeInTextarea(accent);
        updateOutput();
    };

    const scrollHandler = (_event) => {
        output.scrollTop = input.scrollTop;
    }

    input.addEventListener("input", inputHandler);
    input.addEventListener("input", scrollHandler);
    input.addEventListener("scroll", scrollHandler);
    accent_btn.addEventListener("click", insertAccentHandler);
    refresh_btn.addEventListener("click", inputHandler);
    load_btn.addEventListener("click", loadHandler);
})();