use anyhow::Result;
use itertools::Itertools;
use poet_tools_lib::format;
use std::env::args;
use std::fs::File;
use std::io::{stdin, BufRead, BufReader};

fn read_file(reader: impl BufRead) -> Result<()> {
    for line in reader.lines() {
        let line = line?;
        if line.is_empty() {
            println!();
        } else {
            print!("{line}: ");
            println!("{}", format(&line)?);
        }
    }

    Ok(())
}

fn main() -> Result<()> {
    let args = args().skip(1).collect_vec();
    if args.is_empty() {
        let reader = BufReader::new(stdin());
        read_file(reader)?;
    } else {
        for arg in args {
            let file = File::open(arg)?;
            let reader = BufReader::new(file);
            read_file(reader)?;
        }
    }

    Ok(())
}
