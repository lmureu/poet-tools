use poet_tools_lib::{count_metric_syllables, syllabify};

#[test]
fn rigoletto_naive() {
    let source = [
        "La donna è mobile",
        "Qual piuma al vento,",
        "Muta d'accento",
        "e di pensiero.",
        "Sempre un amabile,",
        "Leggiadro viso,",
        "In pianto o in riso,",
        "è menzognero.",
        "È sempre misero",
        "Chi a lei s'affida,",
        "Chi le confida",
        "mal cauto il core!",
        "Pur mai non sentesi",
        "Felice appieno",
        "Chi su quel seno",
        "non liba amore!",
    ];
    let expected = [6, 5, 5, 5, 6, 5, 5, 5, 6, 5, 5, 5, 6, 5, 5, 5];
    let syllables = source.map(|line| syllabify(line).len());

    assert_eq!(expected, syllables);
}
#[test]
fn divina_commedia_naive() {
    let source = [
        "Nel mezzo del cammin di nostra vita",
        "mi ritrovai per una selva oscura,",
        "ché la diritta via era smarrita.",
        "Ahi quanto a dir qual era è cosa dura",
        "esta selva selvaggia e aspra e forte",
        "che nel pensier rinova la paura!",
    ];
    let expected = [11, 11, 10, 11, 10, 10];
    let syllables = source.map(|line| syllabify(line).len());

    assert_eq!(expected, syllables);
}

#[test]
fn divina_commedia_dieresi() {
    let source = [
        "Nel mezzo del cammin di nostra vita",
        "mi ritrovai per una selva oscura,",
        "ché la diritta via ëra smarrita.",
        "Ahi quanto a dir qual era è cosa dura",
        "esta selva selvaggia e äspra e forte",
        "che nel pensier rinova la paüra!",
    ];
    let expected = [11; 6];
    let syllables = source.map(|line| syllabify(line).len());

    assert_eq!(expected, syllables);
}

#[test]
fn rigoletto_accent() {
    let source = [
        "La donna è mo\u{03_32}bile",
        "Qual piuma al ve\u{03_32}nto,",
        "Muta d'acce\u{03_32}nto",
        "e di pensie\u{03_32}ro.",
        "Sempre un ama\u{03_32}bile,",
        "Leggiadro vi\u{03_32}so,",
        "In pianto o in ri\u{03_32}so,",
        "è menzogne\u{03_32}ro.",
        "È sempre mi\u{03_32}sero",
        "Chi a lei s'affi\u{03_32}da,",
        "Chi le confi\u{03_32}da",
        "mal cauto il co\u{03_32}re!",
        "Pur mai non se\u{03_32}ntesi",
        "Felice appie\u{03_32}no",
        "Chi su quel se\u{03_32}no",
        "non liba amo\u{03_32}re!",
    ];
    let expected = [5; 16];
    let syllables = source.map(|line| count_metric_syllables(&syllabify(line)));

    assert_eq!(expected, syllables);
}

#[test]
fn divina_commedia_2() {
    let source = [
        "Io non so ben ridir com'i' v'intra\u{03_32}i,",
        "tant’era pien di sonno ä quel pu\u{03_32}nto",
        "che la verace via äbbandona\u{03_32}i.",
    ];
    let expected = [11; 3];
    let syllables = source.map(|line| count_metric_syllables(&syllabify(line)));

    assert_eq!(expected, syllables);
}
